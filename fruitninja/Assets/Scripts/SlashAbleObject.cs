using UnityEngine;

public class SlashAbleObject : MonoBehaviour
{
    public bool isBomb;
    [field: SerializeField] public int Score { get; private set; } = 10;

    [SerializeField] private Rigidbody rigidBody;


    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        //SpawnObject();
    }


    private void OnBecameInvisible()
    {
        ResetObjectVelocity();

        gameObject.SetActive(false);
    }

    private void ResetObjectVelocity()
    {
        rigidBody.velocity = Vector3.zero;
    }

    public void AddForceToRigidBody(Vector3 force)
    {
        rigidBody.AddForce(force, ForceMode.Force);
    }

    
}