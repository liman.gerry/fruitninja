using System;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    [SerializeField] private int score;
    public event Action<int> ScoreChanged;

    // Start is called before the first frame update
    void Start()
    {
        ResetScore();
    }


    public void AddScore(int objectScore)
    {
        score += objectScore;
        ScoreChanged?.Invoke(score);
    }

    public void ResetScore()
    {
        score = 0;
        ScoreChanged?.Invoke(score);
    }
}