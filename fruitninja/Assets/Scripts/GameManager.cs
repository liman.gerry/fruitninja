using System;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private Player player;

    [SerializeField] private ScoreManager scoreManager;
    private UIManager uiManager;
    [SerializeField] private ObjectSpawner objectSpawner;

    public event Action SlashingBomb;

    public event Action GameOver;


    private void Awake()
    {
        uiManager = FindObjectOfType<UIManager>();
        player ??= FindObjectOfType<Player>();
        scoreManager ??= FindObjectOfType<ScoreManager>();
        objectSpawner ??= FindObjectOfType<ObjectSpawner>();
    }

    // Start is called before the first frame update
    void Start()
    {
        player.SlashingObject += OnSlashingObject;
        player.SlashingBomb += OnSlashingBomb;
        player.GameOver += OnGameOver;
    }

    private void OnDestroy()
    {
        player.SlashingObject -= OnSlashingObject;
        player.SlashingBomb -= OnSlashingBomb;
        player.GameOver -= OnGameOver;
    }

    private void OnGameOver()
    {
        GameOver?.Invoke();
        
        objectSpawner.SetStopSpawning(true);
    }

    private void OnSlashingBomb()
    {
        SlashingBomb?.Invoke();
      
    }


    private void OnSlashingObject(List<GameObject> hits)
    {
        SlashAbleObject slashAbleObject;
        foreach (var hit in hits)
        {
            slashAbleObject = hit.GetComponent<SlashAbleObject>();
            if (slashAbleObject is null)
            {
                continue;
            }

            scoreManager.AddScore(slashAbleObject.Score);
        }
    }

    public void StartGame()
    {
        player.ResetPlayer();
        uiManager.StartGameUI();
        scoreManager.ResetScore();
        objectSpawner.SetStopSpawning(false);
    }

    
}