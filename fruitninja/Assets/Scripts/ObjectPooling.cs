
using System.Collections.Generic;
using UnityEngine;


public class ObjectPooling : MonoBehaviour
{
    [SerializeField] private GameObject[] slashAbleGameObjects;

    [SerializeField] private int objectAmount = 10;

    private List<SlashAbleObject> slashAbleGameObjectsList = new List<SlashAbleObject>();

    public static ObjectPooling sharedInstance;

    private void Awake()
    {
        sharedInstance = this;
        SpawnAllSlashAbleObjects();
    }

    
    

    private void SpawnAllSlashAbleObjects()
    {
        foreach (GameObject slashAbleGameObject in slashAbleGameObjects)
        {
            for (int i = 0; i < objectAmount; i++)
            {
                GameObject instantiatedObject = Instantiate(slashAbleGameObject, transform);
                slashAbleGameObjectsList.Add(instantiatedObject.GetComponent<SlashAbleObject>());
                instantiatedObject.SetActive(false);
            }
        }
    }

    public SlashAbleObject GetRandomPooledObject(bool isBomb)
    {
        foreach (SlashAbleObject slashAbleObject in slashAbleGameObjectsList)
        {
           
            if (!slashAbleObject.gameObject.activeInHierarchy && slashAbleObject.isBomb == isBomb)
            {
                return slashAbleObject;
            }
        }

        return null;
    }
}
