using System;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] public Camera mainCamera;
    private const int baseHealth = 3;
    public event Action<List<GameObject>> SlashingObject;

    public event Action SlashingBomb;
    public event Action GameOver;


    private int health;

    private void Awake()
    {
        mainCamera ??= Camera.main;
    }

    // Start is called before the first frame update
    void Start()
    {
        health = baseHealth;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit[] hits =
                Physics.RaycastAll(mainCamera.ScreenToWorldPoint(Input.mousePosition), Vector3.forward);

            if (hits is null) return;
            List<GameObject> objects = new List<GameObject>();
            foreach (var hit in hits)
            {
                if (hit.collider.CompareTag("NonBomb"))
                {
                    objects.Add(hit.collider.gameObject);
                }
                else
                {
                    DecreaseHealth();
                    SlashingBomb?.Invoke();
                    if (health <= 0)
                    {
                        GameOver?.Invoke();
                    }
                }

                hit.transform.gameObject.SetActive(false);
            }

            if (objects.Count <= 0)
            {
                return;
            }

            SlashingObject?.Invoke(objects);
        }
    }

    private void DecreaseHealth()
    {
        health -= 1;
    }

    public void ResetPlayer()
    {
        health = baseHealth;
    }
}