using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private ScoreManager scoreManager;
    [SerializeField] private GameManager gameManager;
    [SerializeField] private Image[] healthImages;
    [SerializeField] private GameObject gameOverUI;
    [SerializeField] private GameObject gameUI;
    [SerializeField] private GameObject mainMenuUI;

    private void Awake()
    {
        scoreManager = FindObjectOfType<ScoreManager>();
    }

    private void OnEnable()
    {
        gameManager.GameOver += OnGameOver;
        gameManager.SlashingBomb += OnSlashingBomb;
    }

    private void OnSlashingBomb()
    {
        DecreaseHealthUI();
    }

    private void OnGameOver()
    {
        ToggleHealthUI(false);
        ToggleGameOverUI(true);
    }

    // Start is called before the first frame update
    void Start()
    {
        ResetUI();
        scoreManager.ScoreChanged += OnScoreChanged;
    }

    private void OnScoreChanged(int score)
    {
        UpdateScoreUI(score);
    }

    private void UpdateScoreUI(int score)
    {
        scoreText.text = "Score : " + score.ToString("000000");
    }

    public void DecreaseHealthUI()
    {
        foreach (Image healthImage in healthImages)
        {
            if (healthImage.gameObject.activeInHierarchy)
            {
                healthImage.gameObject.SetActive(false);
                return;
            }
        }
    }

    public void ToggleHealthUI(bool isTrue)
    {
        foreach (Image healthImage in healthImages)
        {
            if (!healthImage.gameObject.activeInHierarchy)
            {
                healthImage.gameObject.SetActive(isTrue);
            }
        }
    }

    public void ToggleGameOverUI(bool isTrue)
    {
        gameOverUI.SetActive(isTrue);
    }


    public void StartGameUI()
    {
        gameUI.SetActive(true);
        mainMenuUI.SetActive(false);
        //UpdateScoreUI(0);
        ToggleHealthUI(true);
    }

    public void ResetUI()
    {
        gameUI.SetActive(false);
        mainMenuUI.SetActive(true);
        ToggleGameOverUI(false);
    }
}