using System;
using UnityEngine;
using Random = System.Random;
using RandomUnity = UnityEngine.Random;

public class ObjectSpawner : MonoBehaviour
{
    [SerializeField] private float spawnCooldown = 1.5f;

    [SerializeField] private int maxNumberObjectSpawn = 4;

    [SerializeField] private float minSpawnOffSetX = -6f;
    [SerializeField] private float maxSpawnOffSetX = 6f;
    [SerializeField] private float maxForceOffsetY = 1000f;
    [SerializeField] private float baseForceY = 2000f;
    [SerializeField] private float maxForceOffsetX = 200f;
    [SerializeField] private float baseForceX = 50f;
    private float spawnTimer;
    private bool stopSpawning = true;
    [SerializeField] private Camera mainCamera;

    private void Awake()
    {
        mainCamera ??= Camera.main;
    }

    // Start is called before the first frame update
    void Start()
    {
        spawnTimer = spawnCooldown;
    }


    private void FixedUpdate()
    {
        if (stopSpawning) return;
        spawnTimer -= Time.deltaTime;
        if (spawnTimer <= 0f)
        {
            Random rand = new Random();

            bool randomBool = rand.NextDouble() >= 0.6f;

            float randomXOffSet = RandomUnity.Range(minSpawnOffSetX, maxSpawnOffSetX + 1);
            randomXOffSet += (float)rand.NextDouble();

            SlashAbleObject randomGameSlashAbleObject = SpawnRandomObject(randomBool, randomXOffSet);

            float xForceDirection = 1f;
            if (mainCamera.WorldToViewportPoint(randomGameSlashAbleObject.transform.position).x >= 0.5f)
            {
                xForceDirection = -1f;
            }


            randomGameSlashAbleObject.AddForceToRigidBody(new Vector3(
                xForceDirection * (baseForceX + (maxForceOffsetX * (float)rand.NextDouble())),
                baseForceY + (maxForceOffsetY * (float)rand.NextDouble()), 0f));

            spawnTimer = spawnCooldown;
        }
    }

    private SlashAbleObject SpawnRandomObject(bool randomBool, float randomXOffSet)
    {
        SlashAbleObject randomSlashAbleGameObject = ObjectPooling.sharedInstance.GetRandomPooledObject(randomBool);
        randomSlashAbleGameObject.gameObject.SetActive(true);
        Vector3 spawnPosition = transform.position;
        spawnPosition.x += randomXOffSet;
        randomSlashAbleGameObject.transform.position = spawnPosition;
        return randomSlashAbleGameObject;
    }

    public void SetStopSpawning(bool isTrue)
    {
        stopSpawning = isTrue;
    }
}